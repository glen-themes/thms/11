![Screenshot preview of the theme "Shatter" by glenthemes](https://64.media.tumblr.com/4a7292e478d86a54629f9529e2abe041/tumblr_nw3iclS3pz1ubolzro2_r1_1280.png)

**Theme no.:** 11  
**Theme name:** Shatter  
**Theme type:** Free / Tumblr use  
**Description:** An ultra minimal theme, ft. Tokyo Ghoul.  
**Author:** @&hairsp;glenthemes  

**Release date:** 2015-10-14

**Post:** [glenthemes.tumblr.com/post/131157435019](https://glenthemes.tumblr.com/post/131157435019)  
**Preview:** [glenthpvs.tumblr.com/shatter](https://glenthpvs.tumblr.com/shatter)  
**Download:** [pastebin.com/eThyWjbg](https://pastebin.com/eThyWjbg)
